package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.service.database.exceptions.*;

/**
 * Author: Alexander Fal (falalexandr007@gmail.com)
 */
public class UserTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    
    @Test
    public void testCreateNewUser() throws BadUserException {
        new User.UserData("John", "Wick", "john_wick@gmail.com", "Strpass123", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithEmptyPass() throws BadUserException {
        expectedEx.expect(BadUserPasswordException.class);
        expectedEx.expectMessage(BadUserPasswordException.EMPTY_PASSWORD_MESSAGE);

        new User.UserData("John", "Wick", "john_wick@gmail.com", "", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithShortBadPass() throws BadUserException {
        expectedEx.expect(BadUserPasswordException.class);
        expectedEx.expectMessage(BadUserPasswordException.SHORT_PASSWORD_MESSAGE);

        new User.UserData("John", "Wick", "john_wick@gmail.com", "A2c@d", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithShortGoodPass() throws BadUserException {
        new User.UserData("John", "Wick", "john_wick@gmail.com", "A2c@dw", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithLongBadPass() throws BadUserException {
        expectedEx.expect(BadUserPasswordException.class);
        expectedEx.expectMessage(BadUserPasswordException.LONG_PASSWORD_MESSAGE);

        new User.UserData("John", "Wick", "john_wick@gmail.com", "123QWE123qwe1", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithLongGoodPass() throws BadUserException {
        new User.UserData("John", "Wick", "john_wick@gmail.com", "123QWE123qwe", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithEasyPass() throws BadUserException {
        expectedEx.expect(BadUserPasswordException.class);
        expectedEx.expectMessage(BadUserPasswordException.EASY_PASSWORD_MESSAGE);

        new User.UserData("John", "Wick", "john_wick@gmail.com", "123qwe", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithLoginInPass() throws BadUserException {
        expectedEx.expect(BadUserPasswordException.class);
        expectedEx.expectMessage(BadUserPasswordException.PASSWORD_CONTAINS_LOGIN_MESSAGE);

        new User.UserData("John", "Wick", "a@ya.ru", "Aa@ya.ruS1", User.UserData.UserRole.USER);
    }
    
    @Test
    public void testCreateNewUserWithFirstNameInPass() throws BadUserException {
        expectedEx.expect(BadUserPasswordException.class);
        expectedEx.expectMessage(BadUserPasswordException.PASSWORD_CONTAINS_FIRSTNAME_MESSAGE);

        new User.UserData("John", "Wick", "john_wick@gmail.com", "ADSJohn123", User.UserData.UserRole.USER);
    }
    
    @Test
    public void testCreateNewUserWithLastNameInPass() throws BadUserException {
        expectedEx.expect(BadUserPasswordException.class);
        expectedEx.expectMessage(BadUserPasswordException.PASSWORD_CONTAINS_LASTNAME_MESSAGE);

        new User.UserData("John", "Wick", "john_wick@gmail.com", "As2WickQwe", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithInvalidEmailWithoutDomain() throws BadUserException {
        expectedEx.expect(BadUserLoginException.class);
        expectedEx.expectMessage(BadUserLoginException.INVALID_LOGIN_MESSAGE);

        new User.UserData("John", "Wick", "john_wick", "Strpass123", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithInvalidEmail() throws BadUserException {
        expectedEx.expect(BadUserLoginException.class);
        expectedEx.expectMessage(BadUserLoginException.INVALID_LOGIN_MESSAGE);

        new User.UserData("John", "Wick", "john_wick@@gmail.com", "Strpass123", User.UserData.UserRole.USER);
    }

   
    @Test
    public void testCreateNewUserWithWrongSymbolsInFirstName() throws BadUserException {
        expectedEx.expect(BadUserFirstNameException.class);
        expectedEx.expectMessage(BadUserFirstNameException.WRONG_SYMBOLS_MESSAGE);

        new User.UserData("JoH1n", "Wick", "john_wick@gmail.com", "Strpass123", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithShortBadFirstName() throws BadUserException {
        expectedEx.expect(BadUserFirstNameException.class);
        expectedEx.expectMessage(BadUserFirstNameException.SHORT_FIRSTNAME_MESSAGE);

        new User.UserData("J", "Wick", "john_wick@gmail.com", "Strpass123", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithShortGoodFirstName() throws BadUserException {
        new User.UserData("Jo", "Wick", "john_wick@gmail.com", "Strpass123", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithLongBadFirstName() throws BadUserException {
        expectedEx.expect(BadUserFirstNameException.class);
        expectedEx.expectMessage(BadUserFirstNameException.LONG_FIRSTNAME_MESSAGE);

        new User.UserData("Johnjohnjohnj", "Wick", "john_wick@gmail.com", "Strpass123", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithLongGoodFirstName() throws BadUserException {
        new User.UserData("Johnjohnjohn", "Wick", "john_wick@gmail.com", "Strpass123", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithWrongSymbolsInLastName() throws BadUserException {
        expectedEx.expect(BadUserLastNameException.class);
        expectedEx.expectMessage(BadUserLastNameException.WRONG_SYMBOLS_MESSAGE);

        new User.UserData("John", "Wi1ck", "john_wick@gmail.com", "Strpass123", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithShortBadLastName() throws BadUserException {
        expectedEx.expect(BadUserLastNameException.class);
        expectedEx.expectMessage(BadUserLastNameException.SHORT_LASTNAME_MESSAGE);

        new User.UserData("John", "W", "john_wick@gmail.com", "Strpass123", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithShortGoodLastName() throws BadUserException {
        new User.UserData("John", "Wi", "john_wick@gmail.com", "Strpass123", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithLongBadLastName() throws BadUserException {
        expectedEx.expect(BadUserLastNameException.class);
        expectedEx.expectMessage(BadUserLastNameException.LONG_LASTNAME_MESSAGE);

        new User.UserData("John", "Wickwickwickw", "john_wick@gmail.com", "Strpass123", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithLongGoodLastName() throws BadUserException {
        new User.UserData("John", "Wickwickwick", "john_wick@gmail.com", "Strpass123", User.UserData.UserRole.USER);
    }
}
