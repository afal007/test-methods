package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.exceptions.BadSubscriptionException;
import ru.nsu.fit.endpoint.service.database.exceptions.BadSubscriptionMaxSeatsException;
import ru.nsu.fit.endpoint.service.database.exceptions.BadSubscriptionMinSeatsException;

/**
 * Author: Alexander Fal (falalexandr007@gmail.com)
 */
public class SubscriptionTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewSubscription() throws BadSubscriptionException {
        new Subscription.SubscriptionData(Subscription.SubscriptionData.Status.PROVISIONING);
    }
}

