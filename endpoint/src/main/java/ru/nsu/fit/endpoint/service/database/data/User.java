package ru.nsu.fit.endpoint.service.database.data;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.validator.routines.EmailValidator;

import ru.nsu.fit.endpoint.service.database.exceptions.*;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com), Aleksandr Fal (falalexandr007@gmail.com)
 */
public class User extends Entity<User.UserData> {
    private UUID customerId;
    private UUID[] subscriptionIds;
    private UUID id;

    public User(UserData data, UUID id, UUID customerId) throws BadUserException {
        super(data);
        this.id = id;
        this.customerId = customerId;
        data.validate();
    }

    public UUID getCustomerId() {
        return customerId;
    }
    public void setCustomerId(UUID customerId) {
        this.customerId = customerId;
    }
    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }
    public UUID[] getSubscriptionIds() {
        return subscriptionIds;
    }
    public void setSubscriptionIds(UUID[] ids){
        subscriptionIds = ids;
    }


    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class UserData {
        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
        @JsonProperty("firstName")
        private String firstName;
        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
        @JsonProperty("lastName")
        private String lastName;
        /* указывается в виде email, проверить email на корректность */
        @JsonProperty("login")
        private String login;
        /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
        @JsonProperty("pass")
        private String pass;
        @JsonProperty("userRole")
        private UserRole userRole;

        public String getLastName() {
            return lastName;
        }
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }
        public String getLogin() {
            return login;
        }
        public void setLogin(String login) {
            this.login = login;
        }
        public String getPass() {
            return pass;
        }
        public void setPass(String pass) {
            this.pass = pass;
        }
        public UserRole getUserRole() {
            return userRole;
        }
        public void setUserRole(UserRole userRole) {
            this.userRole = userRole;
        }
        public String getFirstName() {
            return firstName;
        }
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        @Override
        public String toString() {
            return "UserData{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", login='" + login + '\'' +
                    ", pass='" + pass + '\'' +
                    ", userRole=" + userRole +
                    '}';
        }

        public static enum UserRole {
            COMPANY_ADMINISTRATOR("Company administrator"),
            TECHNICAL_ADMINISTRATOR("Technical administrator"),
            BILLING_ADMINISTRATOR("Billing administrator"),
            USER("User");

            private String roleName;

            UserRole(String roleName) {
                this.roleName = roleName;
            }

            public String getRoleName() {
                return roleName;
            }

            public static UserRole fromString(String text) {
                if (text != null) {
                    for (UserRole b : UserRole.values()) {
                        if (text.equalsIgnoreCase(b.roleName)) {
                            return b;
                        }
                    }
                }
                return null;
            }
        }

        public UserData(String firstName, String lastName, String login, String pass, UserRole userRole) throws BadUserException {
            this.firstName = firstName;
            this.lastName = lastName;
            this.login = login;
            this.pass = pass;
            this.userRole = userRole;

            validate(firstName, lastName, login, pass);
        }

        private UserData() {
        }

        private void validate(String firstName, String lastName, String login, String pass) throws BadUserException {
            validatePassword(pass);
            validateLogin(login);
            validateFirstName(firstName);
            validateLastName(lastName);
        }

        private void validate() throws BadUserException {
            validate(firstName, lastName, login, pass);
        }

        private void validatePassword(String pass) throws BadUserException {
            if (pass == null || pass.equals("")) throw new BadUserPasswordException(BadUserPasswordException.EMPTY_PASSWORD_MESSAGE);

            if (pass.length() < 6) throw new BadUserPasswordException(BadUserPasswordException.SHORT_PASSWORD_MESSAGE);

            if (pass.length() > 12) throw new BadUserPasswordException(BadUserPasswordException.LONG_PASSWORD_MESSAGE);

            if (evaluatePassword(pass) <= 50) throw new BadUserPasswordException(BadUserPasswordException.EASY_PASSWORD_MESSAGE);
            System.out.println(login);
            if (pass.toLowerCase().contains(login.toLowerCase()))
                throw new BadUserPasswordException(BadUserPasswordException.PASSWORD_CONTAINS_LOGIN_MESSAGE);
            if (pass.toLowerCase().contains(firstName.toLowerCase()))
                throw new BadUserPasswordException(BadUserPasswordException.PASSWORD_CONTAINS_FIRSTNAME_MESSAGE);
            if (pass.toLowerCase().contains(lastName.toLowerCase()))
                throw new BadUserPasswordException(BadUserPasswordException.PASSWORD_CONTAINS_LASTNAME_MESSAGE);
        }

        private void validateLogin(String login) throws BadUserException {
            if (!EmailValidator.getInstance().isValid(login)) {
                throw new BadUserLoginException(BadUserLoginException.INVALID_LOGIN_MESSAGE);
            }
        }

        private void validateFirstName(String firstName) throws BadUserException {
            if (firstName.length() > 12) {
                throw new BadUserFirstNameException(BadUserFirstNameException.LONG_FIRSTNAME_MESSAGE);
            }

            if (firstName.length() < 2) {
                throw new BadUserFirstNameException(BadUserFirstNameException.SHORT_FIRSTNAME_MESSAGE);
            }

            if (!firstName.matches("^[A-Z][a-z]*")) {
                throw new BadUserFirstNameException(BadUserFirstNameException.WRONG_SYMBOLS_MESSAGE);
            }
        }

        private void validateLastName(String lastName) throws BadUserException {
            if (lastName.length() > 12) {
                throw new BadUserLastNameException(BadUserLastNameException.LONG_LASTNAME_MESSAGE);
            }

            if (lastName.length() < 2) {
                throw new BadUserLastNameException(BadUserLastNameException.SHORT_LASTNAME_MESSAGE);
            }

            if (!lastName.matches("^[A-Z][a-z]*")) {
                throw new BadUserLastNameException(BadUserLastNameException.WRONG_SYMBOLS_MESSAGE);
            }
        }

        private int evaluatePassword(String password) {
            String[] regexChecks = {".*[a-z]+.*", //lower
                    ".*[A-Z]+.*", //upper
                    ".*[\\d]+.*", //digits
                    ".*[@%#&]+.*" //symbols
            };
            int res = 0;

            for (String reg : regexChecks)
                if (password.matches(reg))
                    res += 25;

            return res;
        }
    }
}
