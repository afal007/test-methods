package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Alex on 02.10.2016.
 */
public class BadUserFirstNameException extends BadUserException {
    public static final String WRONG_SYMBOLS_MESSAGE = "First name should begin with upper case letter" +
            " and shouldn't contain any symbols or digits or other uppercase letters.";
    public static final String SHORT_FIRSTNAME_MESSAGE = "First name should be at least 2 symbols.";
    public static final String LONG_FIRSTNAME_MESSAGE = "First name shouldn't be longer than 12 symbols.";

    public BadUserFirstNameException() {
        super();
    }

    public BadUserFirstNameException(String msg) {
        super(msg);
    }
}

