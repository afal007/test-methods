package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Alexandr on 9/26/2016.
 */
public class BadCustomerException extends Exception {
    public BadCustomerException() { super(); }
    public BadCustomerException(String msg) { super(msg); }
}
