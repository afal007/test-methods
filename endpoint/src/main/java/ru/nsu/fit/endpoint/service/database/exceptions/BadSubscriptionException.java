package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Alex on 01.10.2016.
 */
public class BadSubscriptionException extends Exception {
    public BadSubscriptionException() { super(); }
    public BadSubscriptionException(String msg) { super(msg); }
}
