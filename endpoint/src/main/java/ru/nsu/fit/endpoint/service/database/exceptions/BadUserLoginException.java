package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Alex on 02.10.2016.
 */
public class BadUserLoginException extends BadUserException {
    public static final String INVALID_LOGIN_MESSAGE = "Login should be valid email.";

    public BadUserLoginException() { super(); }
    public BadUserLoginException(String msg) { super(msg); }
}