package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Alex on 02.10.2016.
 */
public class BadUserException extends Exception {
    public BadUserException() { super(); }
    public BadUserException(String msg) { super(msg); }
}
