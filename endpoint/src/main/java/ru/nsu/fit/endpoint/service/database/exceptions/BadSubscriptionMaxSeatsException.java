package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Alex on 02.10.2016.
 */
public class BadSubscriptionMaxSeatsException extends BadSubscriptionException {
    public static final String BIG_MAXSEATS_MESSAGE = "Max seats shouldn't be bigger than 9999999.";
    public static final String SMALL_MAXSEATS_MESSAGE = "Max seats shouldn't be less than 1.";

    public BadSubscriptionMaxSeatsException() { super(); }
    public BadSubscriptionMaxSeatsException(String message) { super(message); }
}

