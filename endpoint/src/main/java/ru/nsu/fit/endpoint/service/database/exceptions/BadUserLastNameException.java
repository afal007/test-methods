package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Alex on 02.10.2016.
 */
public class BadUserLastNameException extends BadUserException {
    public static final String WRONG_SYMBOLS_MESSAGE = "Last name should begin with upper case letter" +
            " and shouldn't contain any symbols or digits or other uppercase letters.";
    public static final String SHORT_LASTNAME_MESSAGE = "Last name should be at least 2 symbols.";
    public static final String LONG_LASTNAME_MESSAGE = "Last name shouldn't be longer than 12 symbols.";

    public BadUserLastNameException() { super(); }
    public BadUserLastNameException(String msg) { super(msg); }
}